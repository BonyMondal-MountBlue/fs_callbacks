/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
const fs = require("fs");

const path = require("path");

module.exports = {
  FilehandlingUsingModules: function (filename, callback) {
    if (arguments.length < 2) {
      let err = new Error("Invalid data");
      callback(err);
    } else {
      CreateaDirectory((err, data) => {
        if (err) {
          callback(err);
        } else {
          callback(null, data);
          CreateFile(filename, (err, data) => {
            if (err) {
              callback(err);
            } else {
              callback(null, data);
              DeleteFile(filename, (err, data) => {
                if (err) {
                  callback(err);
                } else {
                  callback(null, data);
                  DeleteDirectory((err, data) => {
                    if (err) {
                      callback(err);
                    } else {
                      callback(null, data);
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  }
};

function CreateaDirectory(callback) {
  fs.mkdir(path.join(__dirname, "directory"), (err) => {
    if (err) {
      callback(new Error("Directory already exists!"));
    } else {
      callback(null, "Directory created successfully!");
    }
  });
}

function CreateFile(filename, callback) {
  fs.writeFile(
    path.join(__dirname, `./directory/${filename}.json`),
    "JSONdata",
    (err) => {
      if (err) {
        callback(err);
      } else {
        callback(null, `${filename}.json File created successfully!`);
      }
    }
  );
}

function DeleteFile(filename, callback) {
  fs.unlink(path.join(__dirname, `./directory/${filename}.json`), (err) => {
    if (err) {
      callback(new Error("File not exists!"));
    } else {
      callback(null, `${filename}.json File deleted successfully!`);
    }
  });
}

function DeleteDirectory(callback) {
  fs.rmdir(path.join(__dirname, "directory"), function (err) {
    if (err) {
      err = new Error("Directory not exists!");
      callback(err);
    } else {
      callback(null, "Successfully removed the empty directory!");
    }
  });
}
