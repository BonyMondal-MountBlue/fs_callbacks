const { FilehandlingUsingModules } = require("../problem1");
/**
 *
 * @returns random file name.
 */
function getRandomString() {
  var randomChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  var result = "";
  for (var i = 0; i < 4; i++) {
    result += randomChars.charAt(
      Math.floor(Math.random() * randomChars.length)
    );
  }
  return result;
}

FilehandlingUsingModules(getRandomString(), (err, data) => {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
});
