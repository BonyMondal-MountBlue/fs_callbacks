const { FileHandlingUsingAsyncFunc } = require("../problem2");

const lipsum = "../../Data/lipsum.txt";

FileHandlingUsingAsyncFunc(lipsum,(err, data) => {
  if (err) {
    console.log(err);
  } else {
    console.log(data);
  }
});
