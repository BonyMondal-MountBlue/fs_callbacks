/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/
const fs = require("fs");

const path = require("path");

module.exports = {
  FileHandlingUsingAsyncFunc: function (lipsum, callback) {
    if (arguments.length < 2) {
      let err = new Error("Invalid user input!");
      callback(err);
    } else {
      ReadFile(lipsum, (err, data) => {
        if (err) {
          callback(err);
        } else {
          let tempdata = data.toUpperCase();
          WriteFile(tempdata, (err) => {
            if (err) {
              callback(err);
            } else {
              ReadNewCreatedFile((err,data) => {
                if (err) {
                  callback(err);
                } else {
                  let tempdata = data
                    .toLowerCase()
                    .replace(/\.+/g, ".|")
                    .replace(/\?/g, "?|")
                    .replace(/\!/g, "!|")
                    .split("|")
                    .join("\n");
                  WriteFile(tempdata, (err, data) => {
                    if (err) {
                      callback(err);
                    } else {
                      ReadNewCreatedFile((err, data) => {
                        if (err) {
                          callback(err);
                        } else {
                          let tempdata = data.split("\n").sort().join("\n");

                          WriteFile(tempdata, (err, data) => {
                            if (err) {
                              callback(err);
                            } else {
                              ReadNewCreatedFile((err, data) => {
                                if (err) {
                                  callback(err);
                                } else {
                                  callback(null, data);
                                  DeleteFile((err, data) => {
                                    if (err) {
                                      callback(err);
                                    } else {
                                      callback(null, data);
                                    }
                                  });
                                }
                              });
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  }
};

function ReadFile(lipsum, callback) {
  fs.readFile(lipsum, "utf-8", (err, data) => {
    if (err) {
      callback(new Error("There is no File!"));
    } else {
      callback(null, data);
    }
  });
}

function WriteFile(tempdata, callback) {
  fs.writeFile(
    path.join(__dirname, "../Data/filenames.txt"),
    tempdata,
    (err) => {
      if (err) {
        callback(err);
      } else {
        callback(null, "File created successfully!");
      }
    }
  );
}

function ReadNewCreatedFile(callback) {
  fs.readFile(
    path.join(__dirname, "../Data/filenames.txt"),
    "utf-8",
    (err, data) => {
      if (err) {
        callback(err);
      } else {
        callback(null, data);
      }
    }
  );
}

function DeleteFile(callback) {
  fs.unlink(path.join(__dirname, "../Data/filenames.txt"), (err) => {
    if (err) {
      callback(new Error("File not exists!"));
    } else {
      callback(null, "File deleted successfully!");
    }
  });
}
